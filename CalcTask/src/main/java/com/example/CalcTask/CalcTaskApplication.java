package com.example.CalcTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalcTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalcTaskApplication.class, args);
	}

}
