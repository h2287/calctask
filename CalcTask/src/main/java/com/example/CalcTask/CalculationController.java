package com.example.CalcTask;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculationController {
    private final CalculationRepository calculationRepository;

    public CalculationController(CalculationRepository calculationRepository) {
        this.calculationRepository = calculationRepository;
    }

    @GetMapping("/calc")
    public Calculation calculate(@RequestParam CalculationType type,
                                 @RequestParam int x,
                                 @RequestParam int y) {
        int result;
        switch (type) {
            case SUM:
                result = x + y;
                break;
            case MULT:
                result = x * y;
                break;
            default:
                throw new IllegalArgumentException("Invalid calculation type: " + type);
        }
        Calculation calculation = new Calculation(type, x, y, result);
        calculationRepository.save(calculation);
        return calculation;
    }
}
